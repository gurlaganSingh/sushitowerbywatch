//
//  GameScene.swift
//  SushiTower
//
//  Created by Parrot on 2019-02-14.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit
import WatchConnectivity
import Firebase
import FirebaseDatabase


class GameScene: SKScene,WCSessionDelegate {
    var countTime = 25
    let timerImage = SKSpriteNode(imageNamed: "life_bg")
    let timerImage2 = SKSpriteNode(imageNamed: "life")
    let randomNumber = Int.random(in: 20 ... 22)
    let randomNumber2 = Int.random(in: 15 ... 16)
    var catPosition = "left"
    var chopstickPositions:[String] = []
    var moveName : String = ""
    var positionX = 70.0
    var timerCount = Timer()
    var gamePause = false
    var playerName : String  = ""
    var score = 0
    var databaseHandle : DatabaseHandle?
   // var ref: DatabaseReference?
    
    var refer: DatabaseReference?
    
    //let randomNumber = Int.random(in: 1 ...10)
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    // Make a tower
    var sushiTower:[SKSpriteNode] = []
    let SUSHI_PIECE_GAP:CGFloat = 80

    // Make chopsticks
    var chopstickGraphicsArray:[SKSpriteNode] = []
    
    // Make variables to store current position
   
    func spawnSushi() {
  
        let sushi = SKSpriteNode(imageNamed:"roll")
        if (self.sushiTower.count == 0) {
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)

        let stickPosition = Int.random(in: 1...2)
        print("Random number: \(stickPosition)")
//         if (stickPosition == 0) {
//         }
         if (stickPosition == 1) {
            // save the current position of the chopstick
            self.chopstickPositions.append("right")
            
            // draw the chopstick on the screen
            let stick = SKSpriteNode(imageNamed:"chopstick")
            stick.position.x = sushi.position.x + 100
            stick.position.y = sushi.position.y - 10
            // add chopstick to the screen
            addChild(stick)
            
            // add the chopstick object to the array
            self.chopstickGraphicsArray.append(stick)
            
            // redraw stick facing other direciton
            let facingRight = SKAction.scaleX(to: -1, duration: 0)
            stick.run(facingRight)
        }
        else if (stickPosition == 2) {
            // save the current position of the chopstick
            self.chopstickPositions.append("left")
            
            // left
            let stick = SKSpriteNode(imageNamed:"chopstick")
            stick.position.x = sushi.position.x - 100
            stick.position.y = sushi.position.y - 10
            // add chopstick to the screen
            addChild(stick)
            
            // add the chopstick to the array
            self.chopstickGraphicsArray.append(stick)
        
        }
    }
    
    override func didMove(to view: SKView) {
        // add background
        //post()
         refer = Database.database().reference()
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        // Do any additional setup after loading the view.
            if (WCSession.isSupported() == true) {
                print("WC is supported!")
                let session = WCSession.default
                session.delegate = self
                session.activate()
            }
            else {
                print ("WC NOT supported!")
            }
        
        timerImage2.position = CGPoint(x: 70, y: 750)
        timerImage2.zPosition = 1
        timerImage.position = CGPoint(x: 70, y: 750)
        addChild(timerImage)
        addChild(timerImage2)
        beginCounter()
       
    }
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
        for i in 0...5 {
           print(self.chopstickPositions[i])
        }
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        self.moveName = message["name"] as! String
        let powerUp = message["powerUp"] as! String
       self.playerName = message["playerName"] as! String
        let gameEnd = message["gameEnd"] as! String
        let gamepause = message["gamePause"] as! String
        self.gamePause = Bool(gamepause)!
//        print(moveName)
//        print(powerUp)
        print(playerName)
        
        
        let pieceToRemove = self.sushiTower.first
        let stickToRemove = self.chopstickGraphicsArray.first
        
        if (pieceToRemove != nil && stickToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // STICK: hide it from screen & remove from game logic
            stickToRemove!.removeFromParent()
            //bjhbhjbjhb 
            self.chopstickGraphicsArray.remove(at:0)
            
            // STICK: Update stick positions array:
            self.chopstickPositions.remove(at:0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // STICK: loop through the remaining sticks and redraw
            for stick in chopstickGraphicsArray {
                stick.position.y = stick.position.y - SUSHI_PIECE_GAP
            }
            
            self.spawnSushi()
        }
        if(moveName == "leftButton"){
            cat.position = CGPoint(x:self.size.width*0.25, y:100)
            
            // change the cat's direction
            let facingRight = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(facingRight)
            
            // save cat's position
            self.catPosition = "left"
        }
        else if(moveName == "rightButton"){
            cat.position = CGPoint(x:self.size.width*0.85, y:100)
            
            // change the cat's direction
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(facingLeft)
            
            // save cat's position
            self.catPosition = "right"
        }
        else if(powerUp == "sendPowerUp"){
            self.countTime = self.countTime + 10
            timerImage2.size = CGSize(width: CGFloat(Int(self.timerImage2.size.width)+55),height: self.timerImage2.size.height)
            
            print(countTime)
        }
        else if(gameEnd == "gameEnd"){
    
            post()
            print(countTime)
        }
    
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
 
        let firstChopstick = self.chopstickPositions[0]
        if (catPosition == "left" && firstChopstick == "left") {
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
        }
        else if (catPosition == "right" && firstChopstick == "right") {
            // you lose
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = LOSE")
            print("------")
        }
        else if (catPosition == "left" && firstChopstick == "right") {
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
            self.score = self.score + 1
            print(score)
            
        }
        else if (catPosition == "right" && firstChopstick == "left") {
            // you win
            print("Cat Position = \(catPosition)")
            print("Stick Position = \(firstChopstick)")
            print("Conclusion = WIN")
            print("------")
            self.score = self.score + 1
            print(score)
            
        }
        

    }
    func post(){

        self.refer?.child("Scores and Name").childByAutoId().setValue(["Name":  self.playerName, "Score": self.score])
    }
    func beginCounter(){
        timerCount = Timer.scheduledTimer(timeInterval: 1.0 , target: self, selector: #selector(countDownTimer), userInfo: nil, repeats: true)
    }
    
    @objc func countDownTimer(){
        if(gamePause == false){
        if(countTime >= 0)
        {
            countTime -= 1
            print(countTime)
            positionX -= 3
            self.score = self.score + 2
            print(score)
           
            timerImage2.size = CGSize(width: CGFloat(Int(self.timerImage2.size.width)-5),height: self.timerImage2.size.height)
            timerImage2.position = CGPoint(x: positionX, y: 750)
            
            if(WCSession.default.isReachable == true){
                let message =  ["leftTime": "\(countTime)","randomNumber" : randomNumber,"randomNumber2": randomNumber2 ] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
                        }
        }
        else{
            print("game over")
//            print("printttttt this")
//            print(playerName)
//            print(score)
                     }
        }
       
    }
    var postDta = [String]()

    override func update(_ currentTime: TimeInterval) {
        databaseHandle = refer?.child("Scores and Name").observe(.childAdded, with: { (DataSnapshot) in
            let post = DataSnapshot.value as? String
            if let actualPost = post{
        self.postDta.append(actualPost)
                print(self.postDta)
            }
        })
    }
 
}
