//
//  InterfaceController.swift
//  sushiTowerWatchKit Extension
//
//  Created by Gurlagan Bhullar on 2019-10-30.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    var score = 0
    var gamePause = false
    @IBOutlet weak var warningLabel: WKInterfaceLabel!
        @IBOutlet weak var savebutton: WKInterfaceButton!
    @IBOutlet weak var pauseButton: WKInterfaceButton!
    
    @IBOutlet weak var leftButtonp: WKInterfaceButton!
    @IBOutlet weak var rightButtonp: WKInterfaceButton!
    @IBOutlet weak var powerUp: WKInterfaceButton!

    @IBAction func powerUpButtonPressed() {
        if(gamePause == false ){
        powerUp.setHidden(true)
        if (WCSession.default.isReachable) {
            let message = ["name":"none","powerUp" : "sendPowerUp", "playerName": "","gamePause" : "false","gameEnd" :"" ] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // print("print this if connection is done")
        }
        }
        
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
            print("is supported")
            
        }
        powerUp.setHidden(true)
        savebutton.setHidden(true)
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func rightButtonPressed() {
         if(gamePause == false ){
        if (WCSession.default.isReachable) {
            let message = ["name":"rightButton","powerUp" : "nothing", "playerName": "","gamePause" : "false","gameEnd" :""] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // print("print this if connection is done")
        }
        }
    }
    @IBAction func leftButtonPressed() {
         if(gamePause == false ){
        if (WCSession.default.isReachable) {
            let message = ["name":"leftButton","powerUp" : "", "playerName": "","gamePause" : "false","gameEnd" :"false"] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // print("print this if connection is done")
        }
        }
    }
    @IBAction func pauseGame() {
        if(gamePause == false){
           
            gamePause = true
            
             print(gamePause)
            pauseButton.setTitle("Resume")
            powerUp.setHidden(true)
            if(WCSession.default.isReachable == true){
                let message =  ["name": "", "powerUp": "",  "playerName": "","gamePause": "true","gameEnd" :""] as [String : Any]
                WCSession.default.sendMessage(message, replyHandler: nil)
            }
            else{
                print("message sent from watch")
            }
        }
        else {
            gamePause = false
            print(gamePause)
            pauseButton.setTitle("pause")
            if(WCSession.default.isReachable == true){
                let message =  ["name": "leftButton", "powerUp": "nothin", "playerName": "", "gamePause": "false","gameEnd" :"" ] as [String : Any]
                WCSession.default.sendMessage(message, replyHandler: nil)
                print("this is output")
            }
            else{
                print("message sent from watch")
            }
        }
        
    }

    
    @IBAction func saveButtonPressed() {
        let suggestedResponses = ["gurlagan", "me"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                let inputName1 = userResponse!
                print(inputName1)
                if(WCSession.default.isReachable == true){
                    let message =  ["name": "", "powerUp": "", "saveScore": "", "playerName": inputName1.prefix(3), "gamePause": "false" , "gameEnd" : "gameEnd" ] as [String : Any]
                    WCSession.default.sendMessage(message, replyHandler: nil)
                    print("message sent to phone from watch")
                }
                else{
                    print("message sent from watch")
                }
                
            }
    }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let leftTime = message["leftTime"] as! String
        print(leftTime)
        let randomNumber = message["randomNumber"] as! Int
        print(randomNumber)
         let randomNumber2 = message["randomNumber2"] as! Int
        print(randomNumber2)
        if(leftTime == "15" || leftTime == "10" || leftTime == "5"){
            self.warningLabel.setText("Warning: \(leftTime) sec left")
        }
        else if (leftTime == "0"){
            self.warningLabel.setText("Game Over!")
            pauseButton.setHidden(true)
            powerUp.setHidden(true)
            savebutton.setHidden(false)
            rightButtonp.setHidden(true)
            leftButtonp.setHidden(true)
        }
        
        if(Int(leftTime) == randomNumber || Int(leftTime) == randomNumber2){
            self.powerUp.setHidden(false)
            
}
        if(Int(leftTime) != randomNumber && Int(leftTime) != randomNumber2){
            self.powerUp.setHidden(true)
            
        }
        
    }
}
